import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CacheLocalStorageModule, CacheModule, MemoryCacheModule } from '@dagonmetric/ng-cache';
import { ConfigModule } from '@dagonmetric/ng-config';
import { HttpConfigLoaderModule } from '@dagonmetric/ng-config/http-loader';
import { LogModule } from '@dagonmetric/ng-log';
import { ApplicationInsightsLoggerModule } from '@dagonmetric/ng-log-applicationinsights';
import { GTagLoggerModule } from '@dagonmetric/ng-log-gtag';
import { LogConfigModule } from '@dagonmetric/ng-log/config';
import { ConsoleLoggerModule } from '@dagonmetric/ng-log/console';
import { TranslitModule } from '@dagonmetric/ng-translit';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,

        ConfigModule.init(),
        HttpConfigLoaderModule,

        LogModule,
        LogConfigModule,
        ConsoleLoggerModule,
        ApplicationInsightsLoggerModule,
        GTagLoggerModule,

        CacheModule,
        MemoryCacheModule,
        CacheLocalStorageModule,

        TranslitModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
